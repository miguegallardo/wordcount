from __future__ import print_function
import sys

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":

    if len(sys.argv) != 2:
    
        print("Usage: spark-submit wordcount <file>", file=sys.stderr)
        exit(-1)

    conf = SparkConf()
    
    cont = SparkContext(conf=conf)

    cont.setLogLevel("OFF")

    output = cont \

        .textFile(sys.argv[1]) \
    
        .flatMap(lambda line: line.split(' ')) \
    
        .map(lambda word: (word, 1)) \
    
        .reduceByKey(lambda a, b: a + b) \
    
        .sortBy(lambda a: a[1], ascending=False) \
    
        .take(30)

    for (word, count) in output:

        print("%s: %i" % (word.encode('utf-8'), count))

    cont.stop()